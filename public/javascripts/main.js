'use strict';


(function(event) {
    event(window.jQuery, document, window);

}(function($, document, window) {

    /********************************************
    //------------------------------------------
    // Helper Functions
    //------------------------------------------
    /********************************************
    /* Captures Jquery value and passes it an anonymous cb func
     * So we can then use other $(Jquery) idenfifiers.
     * We say it "Declares a method on $.fn" where ".fn" is an alias
     * to the "prototype" property in JQuery
     * [JQuery.prototype.methodName === Jquery.fn.methodName === $.fn.methodName] */
    $(function() {
        $.prototype.scrollView = function() {
            return this.each(function() {
                var $root = $('html, body');
                $root.stop().animate({
                    // 60 offset to compensate for navbar
                    scrollTop: $(this).offset().top - 60
                }, 1000, 'swing');
            });
        };

        //---------------------------------------------
        // Helper Function: Empty/Blank String Testing
        //---------------------------------------------
        /* Check if a string is blank or is only made up of white spaces */
        String.prototype.isEmpty = function() {
            return (this.length === 0 || !this.trim());
        };

        //--------------------------------------------
        // Helper Function: Validate Forms recursively
        //--------------------------------------------
        var validateForm = function(name, phone, email) {
            if (name.isEmpty() || phone.isEmpty() || email.isEmpty()) {
                swal({
                    title: "Error",
                    text: "Make sure to fill out all forms!",
                    type: "warning",
                    confirmButtonColor: "#49B571",
                    confirmButtonText: "Okay"
                });
                return validateForm();
            }
        };

        // ----------------------------------------------------------
        //  Smooth Scrolling on click event
        // ----------------------------------------------------------
        // Navigate to respective Div
        var $root = $('html, body');
        $(".navbar-nav li a[href^='#']").on('click', function(e) {
            e.preventDefault();
            var target = this.hash,
                $target = $(target);
            $root.stop().animate({
                'scrollTop': $target.offset().top
            }, 800, 'swing', function() {
                window.location.hash = target;
            });
        });

        // --------------------------------------------------------
        //  Helper: Check for Retina: Returns true if iOS
        // --------------------------------------------------------
        var iOS = /(iPad|iPhone|iPod)/g.test(navigator.userAgent);

        /* Caching */
        var $navbarImg = $(".navbar-brand img");
        var $navBar = $(".navbar");
        var scrollFixedNavbar = 'scroll-fixed-navbar';
        var retinaBlkPBSLogo = 'images/PBSLogo-Blk@2x.png';
        var regBlkPBSLogo = 'images/PBSLogo-Blk.png';
        var regPBSLogo = 'images/PBSLogo.png';
        var scroll = $(window).scrollTop();

        //Fixed size for navbrand img
        $navbarImg.width(240).height(37);
        // ------------------------------------------------------------
        // Scroll Function
        // ------------------------------------------------------------
        $(window).scroll(function() {
            var scroll = $(window).scrollTop();
            if (scroll > 60) {
                $navBar.addClass(scrollFixedNavbar);
                $navbarImg.attr('src', retinaBlkPBSLogo);
            } else if (scroll < 60) {
                $navBar.removeClass(scrollFixedNavbar);
                if ($(window).width() <= 770) {
                    $navbarImg.attr('src', retinaBlkPBSLogo);
                } else {
                    $navbarImg.attr('src', regPBSLogo);
                }
            }
        });

        $(document).ready(function() {
            var bannerHeaderFontDec = parseInt($("#banner-header h1").css('font-size')) - 6;

            // Static Navbar for iOS and higher dense pixel'd screen
            if ($(window).width() <= 450) {
                if (iOS) {
                    $navbarImg.attr('src', retinaBlkPBSLogo);
                } else {
                    $navbarImg.attr('src', regBlkPBSLogo);
                }
                $("#banner-header h1").css('font-size', bannerHeaderFontDec);
            }

            $('#boatsCarousel').carousel('pause');
            $('#upholstery-car').carousel('pause');

            // -----------------------------------------------------------
            // Smooth Scroll: Back to Top
            // -----------------------------------------------------------
            $('.scrollTop').click(function() {
                $('#banner').scrollView();
            });
            // -----------------------------------------------------------
            //  Navigation Bar: Auto Hide menu on click
            // -----------------------------------------------------------
            $('.navbar-collapse').click('li', function() {
                /* Act on the event */
                $('.navbar-collapse').collapse('hide');
            });

            //---------------------------------------------------------
            // Show Respective clicked on btn
            //---------------------------------------------------------
            // Inspections-btn
            $('#inspection-btn').click(function() {
                $('.inspectionsDiv').fadeIn('fast', function() {
                    // Hide all other services
                    $('.maintenanceDiv').fadeOut('fast', function() {
                        $('.canvasDiv').fadeOut('fast', function() {
                            $('.transportDiv').fadeOut('fast', function() {
                                $('.boatWashDiv').fadeOut('fast', function() {
                                    $('.lessonsCont').fadeOut('fast', function() {
                                        //scroll to respective service
                                        $('.teakCont').fadeOut('fast');
                                        $('.inspectionsDiv').scrollView();
                                    });
                                });
                            });
                        });
                    });
                });
            });

            // Maintence-btn
            $('#maintenance-btn').click(function() {
                $('.maintenanceDiv').fadeIn('fast', function() {
                    // Hide all other services
                    $('.inspectionsDiv').fadeOut('fast', function() {
                        $('.canvasDiv').fadeOut('fast', function() {
                            $('.transportDiv').fadeOut('fast', function() {
                                $('.boatWashDiv').fadeOut('fast', function() {
                                    $('.lessonsCont').fadeOut('fast', function() {
                                        //Scroll to respective service
                                        $('.teakCont').fadeOut('fast');
                                        $('.maintenanceDiv').scrollView();
                                    });
                                });
                            });
                        });
                    });
                });
            });

            //Canvas & Upholstery Button
            $('#CanvasDiv-btn').click(function() {
                $('.canvasDiv').fadeIn('fast', function() {
                    // Hide all other services
                    $('.inspectionsDiv').fadeOut('fast', function() {
                        $('.maintenanceDiv').fadeOut('fast', function() {
                            $('.transportDiv').fadeOut('fast', function() {
                                $('.boatWashDiv').fadeOut('fast', function() {
                                    $('.lessonsCont').fadeOut('fast', function() {
                                        //Scroll to respective service
                                        $('.teakCont').fadeOut('fast');
                                        $('.canvasDiv').scrollView();
                                        $('#upholstery-car').carousel('cycle');
                                    });
                                });
                            });
                        });
                    });
                });
            });
            //Transport & Yacht Button
            $('#TransportDiv-btn').click(function() {
                $('.transportDiv').fadeIn('fast', function() {
                    // Hide all other services
                    $('.inspectionsDiv').fadeOut('fast', function() {
                        $('.maintenanceDiv').fadeOut('fast', function() {
                            $('.canvasDiv').fadeOut('fast', function() {
                                $('.boatWashDiv').fadeOut('fast', function() {
                                    $('.lessonsCont').fadeOut('fast', function() {
                                        //Scroll to respective service
                                        $('.teakCont').fadeOut('fast');
                                        $('.transportDiv').scrollView();
                                    });
                                });
                            });
                        });
                    });
                });
            });
            //Boats Wash & Detail Button
            $('#BoatWashDiv-btn').click(function() {
                $('.boatWashDiv').fadeIn('fast', function() {
                    // Hide all other services
                    $('.inspectionsDiv').fadeOut('fast', function() {
                        $('.maintenanceDiv').fadeOut('fast', function() {
                            $('.canvasDiv').fadeOut('fast', function() {
                                $('.transportDiv').fadeOut('fast', function() {
                                    $('.lessonsCont').fadeOut('fast', function() {
                                        //Scroll to respective service
                                        $('.teakCont').fadeOut('fast');
                                        $('.boatWashDiv').scrollView();
                                    });
                                });
                            });
                        });
                    });
                });
            });
            //Boating LEssons
            $('#LessonsDiv-btn').click(function() {
                $('.lessonsCont').fadeIn('fast', function() {
                    // Hide all other services
                    $('.inspectionsDiv').fadeOut('fast', function() {
                        $('.maintenanceDiv').fadeOut('fast', function() {
                            $('.canvasDiv').fadeOut('fast', function() {
                                $('.transportDiv').fadeOut('fast', function() {
                                    $('.boatWashDiv').fadeOut('fast', function() {
                                        //Scroll to respective service
                                        $('.teakCont').fadeOut('fast');
                                        $('.lessonsCont').scrollView();

                                    });
                                });
                            });
                        });
                    });
                });
            });
            //Teak
            $('#TeakDiv-btn').click(function() {
                $('.teakCont').fadeIn('fast', function() {
                    // Hide all other services
                    $('.inspectionsDiv').fadeOut('fast', function() {
                        $('.maintenanceDiv').fadeOut('fast', function() {
                            $('.canvasDiv').fadeOut('fast', function() {
                                $('.transportDiv').fadeOut('fast', function() {
                                    $('.boatWashDiv').fadeOut('fast', function() {
                                        //Scroll to respective service
                                        $('.teakCont').scrollView();
                                    });
                                });
                            });
                        });
                    });
                });
            });
            //---------------------------------------------------------
            // Hide each services,AutoScroll to Services Hash
            //---------------------------------------------------------
            // Finished-btn-inspect
            $('#finished-btn-inspect').click(function() {
                $('#allServices').fadeIn('fast', function() {
                    $('.inspectionsDiv').fadeOut('fast', function() {
                        // Hide all other services
                        $('.maintenanceDiv').fadeOut('fast', function() {
                            $('.canvasDiv').fadeOut('fast', function() {
                                $('.transportDiv').fadeOut('fast', function() {
                                    $('.boatWashDiv').fadeOut('fast', function() {
                                        $('.lessonsCont').fadeOut('fast', function() {
                                            // Scroll
                                            $('#servicesSection').scrollView();
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
            // Finished-btn-main
            $('#finished-btn-main').click(function() {
                $('#allServices').fadeIn('fast', function() {
                    $('.maintenanceDiv').fadeOut('fast', function() {
                        //Fade out all other Divs
                        $('.inspectionsDiv').fadeOut('fast', function() {
                            $('.canvasDiv').fadeOut('fast', function() {
                                $('.transportDiv').fadeOut('fast', function() {
                                    $('.boatWashDiv').fadeOut('fast', function() {
                                        $('.lessonsCont').fadeOut('fast', function() {
                                            //Scroll
                                            $('#servicesSection').scrollView();
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
            // Finished-btn-main2
            $('#finished-btn-main2').click(function() {
                $('#allServices').fadeIn('fast', function() {
                    $('.maintenanceDiv').fadeOut('fast', function() {
                        //Fade out all other Divs
                        $('.inspectionsDiv').fadeOut('fast', function() {
                            $('.canvasDiv').fadeOut('fast', function() {
                                $('.transportDiv').fadeOut('fast', function() {
                                    $('.boatWashDiv').fadeOut('fast', function() {
                                        $('.lessonsCont').fadeOut('fast', function() {
                                            //Scroll
                                            $('#servicesSection').scrollView();
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
            // Finished-btn-canvas
            $('#finished-btn-canvas').click(function() {
                $('#allServices').fadeIn('fast', function() {
                    $('.canvasDiv').fadeOut('fast', function() {
                        //Fade out all other Divs
                        $('.inspectionsDiv').fadeOut('fast', function() {
                            $('.maintenanceDiv').fadeOut('fast', function() {
                                $('.transportDiv').fadeOut('fast', function() {
                                    $('.boatWashDiv').fadeOut('fast', function() {
                                        $('.lessonsCont').fadeOut('fast', function() {
                                            //Scroll
                                            $('#servicesSection').scrollView();
                                            $('#upholstery-car').carousel('pause');
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
            // Finished-btn-trans
            $('#finished-btn-trans').click(function() {
                $('#allServices').fadeIn('fast', function() {
                    $('.transportDiv').fadeOut('fast', function() {
                        //Fade out all other Divs
                        $('.inspectionsDiv').fadeOut('fast', function() {
                            $('.maintenanceDiv').fadeOut('fast', function() {
                                $('.canvasDiv').fadeOut('fast', function() {
                                    $('.boatWashDiv').fadeOut('fast', function() {
                                        $('.lessonsCont').fadeOut('fast', function() {
                                            //Scroll
                                            $('#servicesSection').scrollView();
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
            // Finished-btn-wash
            $('#finished-btn-wash1').click(function() {
                $('#allServices').fadeIn('fast', function() {
                    $('.boatWashDiv').fadeOut('fast', function() {
                        //Fade out all other Divs
                        $('.inspectionsDiv').fadeOut('fast', function() {
                            $('.maintenanceDiv').fadeOut('fast', function() {
                                $('.canvasDiv').fadeOut('fast', function() {
                                    $('.transportDiv').fadeOut('fast', function() {
                                        $('.lessonsCont').fadeOut('fast', function() {
                                            //Scroll
                                            $('#servicesSection').scrollView();
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
            $('#finished-btn-wash2').click(function() {
                $('#allServices').fadeIn('fast', function() {
                    $('.boatWashDiv').fadeOut('fast', function() {
                        //Fade out all other Divs
                        $('.inspectionsDiv').fadeOut('fast', function() {
                            $('.maintenanceDiv').fadeOut('fast', function() {
                                $('.canvasDiv').fadeOut('fast', function() {
                                    $('.transportDiv').fadeOut('fast', function() {
                                        $('.lessonsCont').fadeOut('fast', function() {
                                            //Scroll
                                            $('#servicesSection').scrollView();
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
            // Finished-btn-lessons
            $('#finished-btn-lessons').click(function() {
                $('#allServices').fadeIn('fast', function() {
                    $('.lessonsCont').fadeOut('fast', function() {
                        //Fade out all other Divs
                        $('.inspectionsDiv').fadeOut('fast', function() {
                            $('.maintenanceDiv').fadeOut('fast', function() {
                                $('.canvasDiv').fadeOut('fast', function() {
                                    $('.transportDiv').fadeOut('fast', function() {
                                        $('.boatWashDiv').fadeOut('fast', function() {
                                            //Scroll
                                            $('#servicesSection').scrollView();
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
            // Teak-btn-lessons
            $('#finished-btn-teak').click(function() {
                $('#allServices').fadeIn('fast', function() {
                    $('.teakCont').fadeOut('fast', function() {
                        //Fade out all other Divs
                        $('.inspectionsDiv').fadeOut('fast', function() {
                            $('.maintenanceDiv').fadeOut('fast', function() {
                                $('.canvasDiv').fadeOut('fast', function() {
                                    $('.transportDiv').fadeOut('fast', function() {
                                        $('.boatWashDiv').fadeOut('fast', function() {
                                            //Scroll
                                            $('#servicesSection').scrollView();
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });

        });

        // -------------------------------------------------------
        // Business Logic: Get User's info for email
        // -------------------------------------------------------
        $(function() {
            var name = "",
                email = "",
                phone = "",
                clientInfo = {};

            // Focus Event
            $('#send-clk').click(function(e) {
                e.preventDefault();
                name = $('#banner-name').val();
                phone = $('#banner-phone').val();
                email = $('#banner-email').val();

                // Form Validation
                validateForm(name, phone, email);

                clientInfo = {
                    name: name,
                    phone: phone,
                    email: email
                };

                // Confirmation message for user
                var confirmationMsg = "";
                confirmationMsg += "Please verify your information \n" +
                    "Name: " + clientInfo.name + "\n" +
                    "Email: " + clientInfo.email + "\n" +
                    "Phone Number: " + clientInfo.phone;

                // Chaining SweetAlert instances;
                // Chained to AJAX Call for transactional email from front to backend
                swal({
                        title: "Verify! =)",
                        text: confirmationMsg,
                        type: "info",
                        showCancelButton: true,
                        confirmButtonColor: "#49B571",
                        confirmButtonText: "All is good!",
                        cancelButtonText: "Something isn't quite right...",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            swal({
                                title: "Success",
                                text: "Your inquiry has been sent! You shall receive an email confirmation momentarily",
                                type: "success",
                                confirmButtonColor: "#49B571",
                                confirmButtonText: "Awesome!"
                            });
                            // Ajax HTTP Verb GET call to backend
                            $.get('/send', clientInfo);
                        } else {
                            swal("Cancelled", "We look forward to hearing from you!", "error");
                        }
                    });
            });
        });

        // ------------------------------------------------------------
        // Navigation Bar: Responsive behavior control upon DOM resize
        // ------------------------------------------------------------
        $(window).resize(function() {
            var scroll = $(window).scrollTop();
            if (scroll > 60) {
                $navBar.addClass(scrollFixedNavbar);
                $navbarImg.attr('src', retinaBlkPBSLogo);
            } else if (scroll < 60) {
                $navBar.removeClass(scrollFixedNavbar);
                if ($(window).width() <= 770) {
                    $navbarImg.attr('src', retinaBlkPBSLogo);
                } else {
                    $navbarImg.attr('src', regPBSLogo);
                }
            }
        });
    });
}));
