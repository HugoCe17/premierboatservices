var express = require('express');
var router = express.Router();
var sendgrid = require('sendgrid')('app34137188@heroku.com', 'rl8rnjut');
var hogan = require('hogan.js');
var fs = require('fs');

// get email.hjs using filesystem in node
// compile email.hjs to accept mustache {{}}
var template = fs.readFileSync('./views/email.hjs', 'utf-8');
var compiledTemplate = hogan.compile(template);
var templateConfirmation = fs.readFileSync('./views/emailConfirmation.hjs', 'utf-8');
var compiledTemplateConfirmation = hogan.compile(templateConfirmation);

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index', {
        title: 'Premier Boat Services'
    });
});

/* Send Email upon GET request */
router.get('/send', function(req, res) {
    console.log("Email Controller being called!");

    var name = req.query.name;
    var phone = req.query.phone;
    var email = req.query.email;

    // customer data
    var data = {
        name: name,
        phone: phone,
        email: email
    };

    /****************************************************
     * Send contact info within an Email to PBS Staff
     ****************************************************/
    var emailPBS = new sendgrid.Email({
        //to: "anthony@premierboatservices.com",
        to: "hugoce17@gmail.com",
        toname: "Staff - Premier Boat Services",
        from: email,
        fromname: name,
        subject: "Premier Boat Services [New Inquiry]",
        html: compiledTemplate.render(data)
    });

    console.log(emailPBS);
    sendgrid.send(emailPBS, function(err, json) {
        if (err) {
            console.log(err + 'An Error has Occured!');
        } else {
            console.log(json);
        }
    });

    /****************************************************
     * Send confirmation email to inquirying customer
     ****************************************************/
    var emailCustomer = new sendgrid.Email({
        to: email,
        toname: name,
        from: "<noreply@premierboatservices.com>",
        fromname: "Staff - Premier Boat Services",
        subject: "Confirmation of Inquiry involving Premier Boat Services",
        html: compiledTemplateConfirmation.render(data)
    });

    console.log(emailCustomer);
    sendgrid.send(emailCustomer, function(err, json) {
        if (err) {
            console.log(err + 'An Error has Occured!');
        } else {
            console.log(json);
        }
    });
});

module.exports = router;
